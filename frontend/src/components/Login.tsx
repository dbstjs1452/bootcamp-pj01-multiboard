import React, { Component, useState } from 'react';
import {Button, Modal, Stack, Checkbox, TextField, Box, Grid, Container, FormControlLabel, Typography,InputLabel,FormControl,OutlinedInput,InputAdornment,IconButton} from '@mui/material';
import Avatar from '@mui/material/Avatar';
import LockIcon from '@mui/icons-material/Lock';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import { observer } from 'mobx-react';
import useStores from './../stores/useStores';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';

const Login = observer(()=> {
    const {loginStore} = useStores();
    
    const [inputEmail, setInputEmail] = useState('');
    const [inputPassword, setInputPassword] = useState('');

    const navigate = useNavigate();

    const handleInputEmail = (email: string) => {
        setInputEmail(email);
    }

    const handleInputPassword = (password: string) => {
        setInputPassword(password);
    }

    const [showPassword, setShowPassword] = React.useState(false);

    const handleClickShowPassword = () => setShowPassword((show) => !show);

    const handleMouseDownPassword = (event: React.MouseEvent<HTMLButtonElement>) => {
        event.preventDefault();
    };

    const onClickLogin = async () => {
        console.log("login click!!");
        console.log("Id: ", inputEmail, "/ PW: " , inputPassword);
        
        //const accessToken = await loginStore.login(inputEmail, inputPassword);
        //console.log("accessToken: ", accessToken);

        let loginData = {
            "email" : inputEmail,
            "password" : inputPassword
        }

        axios.post('http://localhost:8090/login', loginData)
        .then(res => {
            localStorage.setItem('token', res.data) 
            localStorage.setItem('userInfo', JSON.stringify(loginStore.decodedToken(res.data)))
            loginStore.setLoggedIn(true)
            alert("logged state(true): "+ loginStore.loggedIn)
            navigate('/')
            
            // 토큰 저장
            //loginStore.setAccessToken(res.data)
            //axios.defaults.headers.common['Authorization'] = `Bearer ${res.data}`
            //axios.get('/user/myInfo').then(res => console.log("My Info: ", res))
        })
        .catch(err => {
            console.log(err);
        })
        
    }

    return (
            <Box
                sx={{
                    marginTop: 8,
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                }}
            >
            <Avatar sx={{ m: 1, bgcolor: 'primary.main', width: '4rem', height:'4rem'}}><LockIcon style={{width: '2rem', height:'2rem'}}/></Avatar>
            <Typography component="h1" variant="h5" marginBottom={3}>
                로그인
            </Typography>
            <Stack spacing={5} minWidth='20rem'>
                <Stack spacing={2}>
                  <TextField
                    autoFocus
                    fullWidth
                    type="email"
                    id="email"
                    name="email"
                    label="이메일 주소"
                    onChange={(event) => handleInputEmail(event.target.value)}
                  />

                    <FormControl  variant="outlined">
                        <InputLabel htmlFor="outlined-adornment-password">비밀번호</InputLabel>
                        <OutlinedInput
                            id="outlined-adornment-password"
                            type={showPassword ? 'text' : 'password'}
                            endAdornment={
                                <InputAdornment position="end">
                                    <IconButton
                                    aria-label="toggle password visibility"
                                    onClick={handleClickShowPassword}
                                    onMouseDown={handleMouseDownPassword}
                                    edge="end"
                                    >
                                    {showPassword ? <VisibilityOff /> : <Visibility />}
                                    </IconButton>
                                </InputAdornment>
                            }
                            label="비밀번호"
                            onChange={(event) => handleInputPassword(event.target.value)}
                        />
                    </FormControl>
                </Stack>
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        sx={{ mt: 5, mb: 2 }}
                        size="large"
                        onClick={onClickLogin}
                    >
                    로그인
                    </Button>
              </Stack>
            </Box>
    ); 
});

export default Login;