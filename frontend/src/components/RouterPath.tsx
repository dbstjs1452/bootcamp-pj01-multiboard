import { Route, Routes } from 'react-router-dom';
import Main from './Main';
import Board from './Board';
import Qna from './Qna';
import Faq from './Faq';
import Notice from './Notice';
import MyInfo from './MyInfo';
import MyList from './MyList';
import Settings from './Settings';
import Login from './Login';
import SignUp from './SignUp';
import PageNotFound from '../PageNotFound';
import CreateBoard from './CreateBoard';
import DetailBoard from './DetailBoard';


const RouterPath = () => {
    return(
        <Routes>
            <Route path='/' element={<Main/>} />
            
            <Route path='/board' element={<Board/>} />
            <Route path='/create-board/:id' element={<CreateBoard />} />
            <Route path='/detail-board/:id' element={<DetailBoard />} />
            
            <Route path='/qna' element={<Qna/>} />
            <Route path='/faq' element={<Faq/>} />
            <Route path='/notice' element={<Notice/>} />

            <Route path='/login' element={<Login/>} />
            <Route path='/signUp' element={<SignUp/>} />

            <Route path='/myInfo' element={<MyInfo/>} />
            <Route path='/myList' element={<MyList/>} /> 
            <Route path='/settings' element={<Settings/>} /> 

            <Route path="*" element={<PageNotFound />}></Route>      
        </Routes>
    )
}

export default RouterPath;