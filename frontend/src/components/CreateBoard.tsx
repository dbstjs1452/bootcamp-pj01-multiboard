import React, { Component, useEffect } from 'react';
import {Paper, Modal, Stack, Checkbox, TextField, Box, Grid, Container, FormControlLabel, Typography} from '@mui/material';
import TwoButtonView from '../share/TwoButtonView';
import { useNavigate, useParams } from 'react-router-dom';
import { BoardType } from '../stores/BoardStore';
import { observer } from 'mobx-react';
import useStores from './../stores/useStores';


const CreateBoard = observer(()=> {

    const {boardStore} = useStores();
    const {board, addBoard, modifyBoard, setOneBoard} = boardStore;
    
    const onSetBoardProps = (name: string, value: string) => {
        boardStore.setBoardProps(name, value);
    }

    const navigate = useNavigate();
    const params = useParams();

    useEffect(() => {
        if(params.id === "_create"){
            //boardStore.resetBoard();
            // board.title = "";
            // board.content = "";
        }else{
            setOneBoard(params.id!);
        }
    },[]);

    const onClickCancelButton = () => {
        navigate('/board');
    }

    const onClickCreateButton = (event: React.MouseEvent<HTMLElement>) => {
        event.preventDefault();
        if(params.id === '_create') {
            addBoard(board);
        }
        else{
            modifyBoard(params.id!, board);
        }
        navigate('/board');
    }

    return (
        <form noValidate>
        <Container component="main" maxWidth="md">
            <Stack sx={{marginTop:8}} spacing={3} >
                <Paper sx={{ width: '100%', mb: 2, padding: '2rem'}}>
                    <Stack spacing={2}>
                        <Stack spacing={1}>
                            <Typography align='left'>글 제목</Typography>
                            <TextField
                                required
                                autoFocus
                                fullWidth
                                id="title"
                                name="title"
                                value={board && board.title ? board.title : '' }
                                onChange={ (event) => onSetBoardProps('title', event.target.value)}
                            />
                        </Stack>
                        <Stack spacing={1}>
                            <Typography align='left'>글 내용</Typography>
                            <TextField
                                required
                                fullWidth
                                multiline
                                maxRows={10}
                                minRows={10}
                                id="content"
                                name="content"
                                value={board && board.content ? board.content : '' }
                                onChange={ (event) => onSetBoardProps('content', event.target.value)}
                            />
                        </Stack>
                        <Stack spacing={1}>
                            <Typography align='left'>글 작성자</Typography>
                            <TextField
                                required
                                autoFocus
                                fullWidth
                                id="writer"
                                name="writer"
                                value={board && board.writer ? board.writer : '' }
                                onChange={ (event) => onSetBoardProps('writer', event.target.value)}
                            />
                        </Stack>
                    </Stack>
                </Paper>
                <TwoButtonView 
                    leftLabel="취소"
                    onClickLeftButton={onClickCancelButton}
                    rightLabel="등록"
                    onClickRightButton={onClickCreateButton}/>
            </Stack>  
        </Container>
        </form>
    );
})

export default CreateBoard;