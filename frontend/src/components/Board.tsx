import React, { useEffect } from 'react';
import {Box,Grid,Table, TableBody, TableCell, TableContainer, TableHead, TablePagination, TableRow, TableSortLabel, Button} from '@mui/material';
import Paper from '@mui/material/Paper';
import CreateIcon from '@mui/icons-material/Create';
import { visuallyHidden } from '@mui/utils';
import { useNavigate } from 'react-router-dom';
import MainTilte from '../share/MainTitle';
import { observer } from "mobx-react";
import useStores from "../stores/useStores";
import { BoardType } from '../stores/BoardStore';


function descendingComparator<T>(a: T, b: T, orderBy: keyof T) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

type Order = 'asc' | 'desc';

function getComparator<Key extends keyof any>(
  order: Order,
  orderBy: Key,
): (
  a: { [key in Key]: number | string },
  b: { [key in Key]: number | string },
) => number {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort<T>(array: readonly T[], comparator: (a: T, b: T) => number) {
  const stabilizedThis = array.map((el, index) => [el, index] as [T, number]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) {
      return order;
    }
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

interface HeadCell {
  id: keyof BoardType;
  label: string;
  numeric: boolean;
}

const headCells: readonly HeadCell[] = [
  {
    id: 'title',
    numeric: false,
    label: '제목',
  },
  {
    id: 'writer',
    numeric: true,
    label: '작성자',
  },
  {
    id: 'createDate',
    numeric: true,
    label: '작성일',
  },
  {
    id: 'hits',
    numeric: true,
    label: '조회수',
  },
];

interface EnhancedTableProps {
  onRequestSort: (event: React.MouseEvent<unknown>, property: keyof BoardType) => void;
  order: Order;
  orderBy: string;
}

function EnhancedTableHead(props: EnhancedTableProps) {
  const { order, orderBy, onRequestSort } =
    props;
  const createSortHandler =
    (property: keyof BoardType) => (event: React.MouseEvent<unknown>) => {
      onRequestSort(event, property);
    };

  return (
    <TableHead>
      <TableRow>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? 'right' : 'left'}
            sortDirection={orderBy === headCell.id ? order : false}
            style={{fontWeight:'bold'}}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <Box component="span" sx={visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </Box>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}


const Board = observer(()=> {
  
  const {boardStore} = useStores();
  
  useEffect(() => {
    boardStore.setBoardList();
  }, []);

  const rows: BoardType[] = boardStore.boards;
  
  const [order, setOrder] = React.useState<Order>('asc');
  const [orderBy, setOrderBy] = React.useState<keyof BoardType>('createDate');
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);


  const handleRequestSort = (
    event: React.MouseEvent<unknown>,
    property: keyof BoardType,
  ) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const navigate = useNavigate();
  const createBoard = () => 
  {
    navigate('/create-board/_create');
  };

  const readBoard = (board: BoardType) => {
    boardStore.selectedBoard(board);
    navigate(`/detail-board/${board.id}`);
  }


  // Avoid a layout jump when reaching the last page with empty rows.
  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - rows.length) : 0;

  return (
    <Box sx={{ width: '100%' }}>
      <Paper sx={{ width: '100%', mb: 2 }}>
        <MainTilte id='tableTitle' titleName='게시판' />
        <TableContainer>
          <Table
            sx={{ minWidth: 500 }}
            aria-labelledby="tableTitle"
          >
            <EnhancedTableHead
              order={order}
              orderBy={orderBy}
              onRequestSort={handleRequestSort}
            />
            <TableBody>
              {
                Array.isArray(rows) && rows.length?
                stableSort(rows, getComparator(order, orderBy))
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((board, index) => {
                    return (
                      <TableRow
                        hover
                        key={board.id}
                        onClick={ () => readBoard(board)}
                      >
                        <TableCell align="left">{board.title}</TableCell>
                        <TableCell align="right">{board.writer}</TableCell>
                        <TableCell align="right">{board.createDate}</TableCell>
                        <TableCell align="right">{board.hits}</TableCell>
                      </TableRow>
                    );
                  })
                  :<TableRow>
                    <TableCell colSpan={4} align="center">
                      작성된 게시글이 없습니다.
                    </TableCell>
                  </TableRow>
              }
              {emptyRows > 0 && (
                <TableRow>
                  <TableCell colSpan={4} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Paper>
      <Grid container justifyContent="flex-end">
        <Button variant='contained' color='primary' startIcon={<CreateIcon />}
            onClick={createBoard}>글쓰기</Button>
      </Grid>
    </Box>
  );
});

export default Board;