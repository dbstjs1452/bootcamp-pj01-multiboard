import React, { Component } from 'react';

class Main extends Component{
    render(): React.ReactNode {
        return <h3>Welcome to BootCamp's MultiBoard made by JYS.</h3>
    }
}
export default Main;