import React, { Component, useEffect, useState } from 'react';
import {Button, Modal, Stack, Checkbox, TextField, Box, Grid, Container, FormControlLabel, Typography} from '@mui/material';
import Avatar from '@mui/material/Avatar';
import { useForm } from 'react-hook-form';
import useStores from '../stores/useStores';
import { observer } from 'mobx-react';
import { UserInfo } from '../stores/UserStore';
import { useNavigate } from 'react-router-dom';

// interface signUpForm{
//   email: string;
//   password: string;
//   checkPassword: string;
//   name: string;
//   phoneNumber: string;
// }

// function SignUp() {
//   const{
//     register,
//     control,
//     handleSubmit: onSubmit,
//     watch,
//     formState: { errors },
//   } = useForm<signUpForm>({
//     mode: "onSubmit",
//     defaultValues:{
//       email: "",
//       password: "",
//       checkPassword: "",
//       name: "",
//       phoneNumber: "",
//     },
//   });

//   const handleSubmit = (data: signUpForm) => {
//     // console.log(data);
//   }

//   return(
//     <form onSubmit={onSubmit(handleSubmit)}>

//     </form>
//   );
// }

const SignUp = observer(()=> {
  
  const {userStore} = useStores();
  const {inputText, isValidEmail, isValidPassword, doubleCheckPassword, isValidName, isValidPhoneNumber, addUser, resetForm, autoPhoneNumber} = userStore;
  
  const [isEmail, setIsEmail] = useState<boolean>(false);
  const [isPassword, setIsPassword] = useState<boolean>(false);
  const [checkRePassword, setCheckRePassword] = useState<boolean>(false);
  const [isName, setIsName] = useState<boolean>(false);
  const [isPhoneNumber, setIsPhoneNumber] = useState<boolean>(false);
  // useEffect(() => {
  //   userStore._inputText.email = "";
  //   userStore._inputText.password = "";
  //   userStore._inputText.checkPassword = "";
  //   userStore._inputText.name = "";
  //   userStore._inputText.phoneNumber = "";
  // }, []);

  // useEffect(() => {
  //   console.log("useEffect!");
  // }, [isEmail,isPassword,checkRePassword,isName,isPhoneNumber])
  
  const navigate = useNavigate();

  const onSetSignUpProps = (name: string, value: string) => {
    userStore.setSignUpProps(name, value);
  }

  // const user : UserInfo = {
  //   email : inputText.email,
  //   password : inputText.password,
  //   name : inputText.name,
  //   phoneNumber : inputText.phoneNumber,
  //   role : ""
  // }
  useEffect(()=>{
    if(isEmail&&isPassword&&checkRePassword&&isName&&isPhoneNumber){
      console.log("회원가입 성공");
      let user : UserInfo = {
        email : inputText.email,
        password : inputText.password,
        name : inputText.name,
        phoneNumber : inputText.phoneNumber,
        role : ""
      }
      addUser(user);
      navigate('/login');
    }
    else{
      console.log("회원가입 실패");
    }
  },[isEmail,isPassword,checkRePassword,isName,isPhoneNumber])

  useEffect(() => {
    resetForm();
    // if(inputText.phoneNumber){
    //   autoPhoneNumber(inputText.phoneNumber)
    // };
  }, []);

  const onClickSignUp = (event: React.MouseEvent<HTMLElement>) => {
    event.preventDefault();
    console.log("Form>> ", JSON.stringify(inputText));
    
    if(isValidEmail(inputText.email)){
      console.log("1111");
      setIsEmail(true);
    };
    if(isValidPassword(inputText.password)){
      setIsPassword(true);
    };
    if(doubleCheckPassword(inputText.password, inputText.checkPassword)){
      setCheckRePassword(true);
    };
    if(isValidName(inputText.name)){
      setIsName(true);
    }
    if(isValidPhoneNumber(inputText.phoneNumber)){
      setIsPhoneNumber(true);
    };
  }

    return (
        <Box
          sx={{
              marginTop: 8,
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
          }}
        >
        <Avatar sx={{ m: 1, bgcolor: 'primary.main', width: '4rem', height: '4rem'}} />
        <Typography component="h1" variant="h5" marginBottom={3}>
            회원가입
        </Typography>
        <Stack spacing={2} minWidth='20rem'>
            <Grid item xs={12}>
              <TextField
                required
                autoFocus
                fullWidth
                type="email"
                id="email"
                name="email"
                label="이메일 주소"
                value={inputText && inputText.email ? inputText.email : '' }
                onChange={ (event) => onSetSignUpProps('email', event.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                required
                fullWidth
                type="password"
                id="password"
                name="password"
                label="비밀번호 (8자리 이상)"
                value={inputText && inputText.password ? inputText.password : '' }
                onChange={ (event) => onSetSignUpProps('password', event.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                required
                fullWidth
                type="password"
                id="rePassword"
                name="rePassword"
                label="비밀번호 재입력"
                value={inputText && inputText.checkPassword ? inputText.checkPassword : '' }
                onChange={ (event) => onSetSignUpProps('checkPassword', event.target.value)}
              />
            </Grid>

            <Grid item xs={12}>
              <TextField
                required
                fullWidth
                id="name"
                name="name"
                label="이름"
                value={inputText && inputText.name ? inputText.name : '' }
                onChange={ (event) => onSetSignUpProps('name', event.target.value)}
              />
            </Grid>

            <Grid item xs={12}>
              <TextField
                required
                fullWidth
                id="phoneNumber"
                name="phoneNumber"
                label="휴대폰 번호"
                value={inputText && inputText.phoneNumber ? inputText.phoneNumber : '' }
                onChange={ (event) => onSetSignUpProps('phoneNumber', event.target.value)}
              />
            </Grid>
            
            <Grid item xs={12}>
              <FormControlLabel
                control={<Checkbox color="primary" />}
                label="회원가입 약관에 동의합니다."
              />
            </Grid>

            <Grid item xs={12}>
                <Button
                    // type="submit"
                    fullWidth
                    variant="contained"
                    sx={{ mt: 3, mb: 2 }}
                    size="large"
                    onClick={onClickSignUp}
                >
                회원가입
                </Button>
            </Grid>
          </Stack>
        </Box>
    ); 
});
export default SignUp;