import * as React from 'react';
import { styled, useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import {Drawer, Avatar, Toolbar, List, Typography, Divider, IconButton, ListItem, ListItemIcon, ListItemText,   } from '@mui/material';
import CssBaseline from '@mui/material/CssBaseline';
import MuiAppBar, { AppBarProps as MuiAppBarProps } from '@mui/material/AppBar';
import MenuIcon from '@mui/icons-material/Menu';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import LoginIcon from '@mui/icons-material/Login';
import LogoutIcon from '@mui/icons-material/Logout';
import PersonAddIcon from '@mui/icons-material/PersonAdd';
import ManageAccountsIcon from '@mui/icons-material/ManageAccounts';
import ListAltIcon from '@mui/icons-material/ListAlt';
import SettingsIcon from '@mui/icons-material/Settings';
import HomeIcon from '@mui/icons-material/Home';
import { NavLink, useNavigate } from 'react-router-dom';
import RouterPath from './RouterPath';
import { observer } from 'mobx-react';
import useStores from './../stores/useStores';
import { UserInfo } from '../stores/LoginStore';

const drawerWidth = 240;

const Nav = styled('nav', { shouldForwardProp: (prop) => prop !== 'open' })<{
  open?: boolean;
}>(({ theme, open }) => ({
  flexGrow: 1,
  padding: theme.spacing(3),
  transition: theme.transitions.create('margin', {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  marginLeft: `-${drawerWidth}px`,
  ...(open && {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: 0,
  }),
}));

interface AppBarProps extends MuiAppBarProps {
  open?: boolean;
}

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== 'open',
})<AppBarProps>(({ theme, open }) => ({
  transition: theme.transitions.create(['margin', 'width'], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: `${drawerWidth}px`,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

const DrawerHeader = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(0, 1),
  // necessary for content to be below app bar
  ...theme.mixins.toolbar,
  justifyContent: 'flex-end',
}));

const PersistentDrawerLeft = observer(()=> {
  const {loginStore} = useStores();

  const theme = useTheme();
  const [open, setOpen] = React.useState(false);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const navigate = useNavigate();

  const onClicklogout = () => {
    localStorage.getItem('token')
    localStorage.clear()
    loginStore.setLoggedIn(false)
    alert("logged state(false): "+loginStore.loggedIn)
    navigate('/')
  }

  const sidebarTopContentsForAll = [
    {
        title: "로그인",
        link: "/login",
        icon: <LoginIcon />,
    },
    {
        title: "회원가입",
        link: "/signUp",
        icon: <PersonAddIcon />,
    },
  ];

  const sidebarTopContentsForUser = [
    {
        title: "로그아웃",
        link: "/logout",
        icon: <LogoutIcon />,
    },
  ];

  const sidebarBottomContents = [
    {
      title: "Home",
      link: "/",
      icon: <HomeIcon />,
    },
    {
        title: "내 정보",
        link: "/myInfo",
        icon: <ManageAccountsIcon />,
    },
    {
        title: "내가 쓴 게시글",
        link: "/myList",
        icon: <ListAltIcon />,
    },
    {
        title: "설정",
        link: "/settings",
        icon: <SettingsIcon />,
    },
];

const userInfo: UserInfo = loginStore.getUser();

console.log();
  return (
    <Box sx={{ display: 'flex' }}>
      <CssBaseline />
      <AppBar position="fixed" open={open}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="end"
            sx={{ mr: 2, ...(open && { display: 'none' }) }}
          >
            <MenuIcon />
          </IconButton>
          <Typography align="left" variant="h6" noWrap sx={{ flexGrow: 1 }} component="div">
            Multi-Board
          </Typography>
            <ul className='nav-links'>
                <li><NavLink to='/board' style={{color:'inherit', textDecoration:'none'}}>게시판</NavLink></li>
                <li><NavLink to='/faq' style={{color:'inherit', textDecoration:'none'}}>FAQ</NavLink></li>
                <li><NavLink to='/qna' style={{color:'inherit', textDecoration:'none'}}>Q&A</NavLink></li>
                <li><NavLink to='/notice' style={{color:'inherit', textDecoration:'none'}}>공지사항</NavLink></li>
            </ul>
        </Toolbar>
        
      </AppBar>
      <Drawer
        sx={{
          width: drawerWidth,
          flexShrink: 0,
          '& .MuiDrawer-paper': {
            width: drawerWidth,
            boxSizing: 'border-box',
          },
        }}
        variant="persistent"
        anchor="left"
        open={open}
      >
        <DrawerHeader>
          {
          (loginStore.loggedIn == true) ?
            <h5>{userInfo.name}님, 환영합니다!</h5>
            : <h5>로그인 후 이용해주세요.</h5>
          }
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
          </IconButton>
        </DrawerHeader>
        <Divider />
        <Avatar
            src='images/profile.png' 
            sx={{ width: 200, height: 200, margin:1, alignSelf:'center'}}/>
        <Divider />
        <List>
            {
              (loginStore.loggedIn == false) ?
              sidebarTopContentsForAll.map((item, index) => (
                <ListItem button key={index} component={NavLink} to={item.link}>
                    <ListItemIcon>{item.icon}</ListItemIcon>
                    <ListItemText primary={item.title} />
                </ListItem>
              ))
              :
              sidebarTopContentsForUser.map((item, index) => (
                <ListItem button key={index} onClick={onClicklogout}>
                    <ListItemIcon>{item.icon}</ListItemIcon>
                    <ListItemText primary={item.title} />
                </ListItem>
              ))
            }
            {/* { sidebarTopContentsForUser.map((item, index) => (
                <ListItem button key={index} onClick={onClicklogout}>
                    <ListItemIcon>{item.icon}</ListItemIcon>
                    <ListItemText primary={item.title} />
                </ListItem>
              ))} */}
              {/* {
                sidebarTopContentsForAll.map((item, index) => (
                  <ListItem button key={index} component={NavLink} to={item.link}>
                      <ListItemIcon>{item.icon}</ListItemIcon>
                      <ListItemText primary={item.title} />
                  </ListItem>
                ))
              } */}
        </List>
        <Divider />
        <List>
            {sidebarBottomContents.map((item, index) => (
                <ListItem button key={index} component={NavLink} to={item.link}>
                    <ListItemIcon>{item.icon}</ListItemIcon>
                    <ListItemText primary={item.title} />
                </ListItem>
            ))}
        </List>
      </Drawer>
      <Nav open={open}>
        <DrawerHeader />
        <RouterPath />
      </Nav>
    </Box>
  );
});

export default PersistentDrawerLeft;
