import React, { Component } from 'react';
import {Divider, Container, Paper, Stack, Typography, Grid} from '@mui/material';
import { observer } from 'mobx-react';
import useStores from './../stores/useStores';
import TwoButtonView from './../share/TwoButtonView';
import { useNavigate, useParams } from 'react-router-dom';
import OneButtonView from '../share/OneButtonView';


const DetailBoard = observer(()=> {
    
    const {boardStore} = useStores();
    const {board} = boardStore;
    const navigate = useNavigate();
    const params = useParams();

    const onClickGoToListButton = () => {
        navigate('/board');
    }

    const onClickGoUpdateButton = () => {
        navigate('/create-board/' + params.id);
    }

    const onClickDeleteButton = () => {
        if(window.confirm("게시글을 삭제할까요?")){
            boardStore.removeBoard(params.id);
            navigate('/board');
        }
    }

    return(

            <Stack sx={{marginTop:8}} spacing={3} >
                <Paper sx={{ width: '100%', mb: 2, padding: '2rem'}}>
                <Stack spacing={1}>
                    <Typography align='left' variant='h5'>{board.title}</Typography>
                    <Stack direction='row' justifyContent="space-between">
                        <p>{board.writer}</p>
                        <p>조회수 {board.hits} | 작성일 {board.createDate}</p>
                    </Stack>
                </Stack>
                <Divider />

                    <pre>
                        {board.content}
                    </pre>

                </Paper>
                <TwoButtonView 
                    leftLabel="삭제"
                    onClickLeftButton={onClickDeleteButton}
                    rightLabel="수정"
                    onClickRightButton={onClickGoUpdateButton}/>
                <OneButtonView 
                    buttonLabel="목록"
                    onClickButton={onClickGoToListButton}/>
            </Stack>

    )
});

export default DetailBoard;