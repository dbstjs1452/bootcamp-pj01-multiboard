import BoardStore from "./BoardStore";
import NoticeStore from "./NoticeStore";
import LoginStore from './LoginStore';
import UserStore from "./UserStore";

export class RootStore{
    
    boardStore: BoardStore;
    noticeStore: NoticeStore;
    loginStore: LoginStore;
    userStore: UserStore;

    constructor(){
        this.boardStore = new BoardStore(this);
        this.noticeStore = new NoticeStore(this);
        this.loginStore = new LoginStore(this);
        this.userStore = new UserStore(this);
    }
}

export const store = new RootStore();