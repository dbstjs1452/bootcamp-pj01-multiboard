import { RootStore } from ".";
import { makeObservable, action, observable } from "mobx";

/* mobX 테스트 중 */
class NoticeStore{
    readonly rootStore: RootStore;
    _test: string = 'helloNotice';

    constructor(rootStore: RootStore){
        this.rootStore = rootStore;
        makeObservable(this,{
            _test: observable,
            changeTestValue: action
        });
    }

    get test(): string{
        return this._test;
    }

    changeTestValue = (value: string) => {
        this._test = value;
    }
}

export default NoticeStore;