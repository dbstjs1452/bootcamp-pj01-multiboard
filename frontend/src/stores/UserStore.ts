import { action, makeObservable, observable } from "mobx";
import { RootStore } from ".";
import UserService from "../service/UserService";

export type SignUpForm = {
  email: string;
  password: string;
  checkPassword: string;
  name: string;
  phoneNumber: string;
}

export type UserInfo = {
    email: string;
    password: string;
    name: string;
    phoneNumber: string;
    role: string;
}

class UserStore{
    readonly rootStore: RootStore;
    _inputText!: SignUpForm;

    isEmail: boolean = false;
    isPassword: boolean = false;
    checkRePassword: boolean = false;
    isName: boolean = false;
    isPhoneNumber: boolean = false;

    constructor(rootStore: RootStore){
        this.rootStore = rootStore;

        makeObservable(this,{
            _inputText: observable,
            isEmail: observable,
            isPassword: observable,
            checkRePassword: observable,
            setSignUpProps: action,
            isValidEmail: action,
            isValidPassword: action,
            doubleCheckPassword: action,
            isValidName: action,
            isValidPhoneNumber: action,
            addUser: action,
            resetForm: action,
            autoPhoneNumber: action,
        });
    }

    get inputText(){
        return this._inputText;
    }

    setSignUpProps(name: string, value: string){
        this._inputText = {
            ...this._inputText!,
            [name] : value
        }
    }

    isValidEmail = (email: string): boolean => {
        const regex = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;
        if(!email){
            console.log("이메일을 입력해주세요.");
            return false;
        }
        else if(email&&!regex.test(email)){
          console.log("이메일 형식이 맞지 않습니다.");
          return false;
        }else{
            this.isEmail = true;
            return true;
        }
    }

    isValidPassword = (password: string): boolean => {
        if(!password){
            console.log("비밀번호를 입력해주세요.");
            return false;
        }
        else if(password&&password.length<8){
            console.log("비밀번호 8자이상 입력해주세요.");
            return false;
        }else{
            this.isPassword = true;
            return true;
        }
    }

    doubleCheckPassword = (password: string, checkPassword: string): boolean => {
        if(password !== checkPassword){
            console.log("비밀번호가 일치하지 않습니다.");
            return false;
        }else{
            this.checkRePassword = true;
            return true;
        }
    }

    isValidName = (name: string): boolean => {
        if(!name){
            console.log("이름을 입력해주세요.");
            return false;
        }
        else if(name&&name.length <= 1){
            console.log("이름은 2자이상 입력해주세요.");
            return false;
        }else{
            this.isName = true;
            return true;
        }
    }

    isValidPhoneNumber = (phoneNumber: string): boolean => {
        const regex = /^01(?:0|1|[6-9])-(?:\d{3}|\d{4})-\d{4}$/;
        if(!phoneNumber){
            console.log("휴대폰번호를 입력해주세요.");
            return false;
        } 
        else if(!regex.test(phoneNumber)){
            console.log("휴대폰번호(-포함)");
            return false
        }else{
            this.isPhoneNumber = true;
            return true;
        }
    }

    autoPhoneNumber = (phoneNumber: string) => {
        if (phoneNumber.length === 10) {
            this._inputText.phoneNumber.replace(/(\d{3})(\d{3})(\d{4})/, '$1-$2-$3');
        }
        if (phoneNumber.length === 13) {
            this._inputText.phoneNumber.replace(/-/g, '').replace(/(\d{3})(\d{4})(\d{4})/, '$1-$2-$3');
        }
    }

    resetForm = () => {
        // this._inputText.email = "";
        // this._inputText.password = "";
        // this._inputText.checkPassword = "";
        // this._inputText.name = "";
        // this._inputText.phoneNumber = "";
        this._inputText={
            email:"",
            password:"",
            checkPassword:"",
            name:"",
            phoneNumber:""
        }
    }

    addUser(user: UserInfo){
        //this.resetForm();
        UserService.createUser(user);
    }
}
export default UserStore;