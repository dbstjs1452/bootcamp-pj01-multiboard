import { RootStore } from ".";
import { makeObservable, action, computed, observable, toJS, flow, runInAction } from "mobx";
import BoardService from "../service/BoardService";
import NameValue from "../type/NameValue";
import NameValueList from '../type/NameValueList';


export type BoardType = {
    id: string,
    title: string;
    content: string,
    writer: string;
    hits: number;
    createDate: string;
    updateDate: string;
}

class BoardStore{
    readonly rootStore: RootStore;
    _board!: BoardType;
    _boards: BoardType[] = [];

    nameValues: NameValue[] = [];

    constructor(rootStore: RootStore){
        this.rootStore = rootStore;

        makeObservable(this,{
            _board: observable,
            _boards: observable,
            boards: computed,
            addBoard: action,
            modifyBoard: action,
            removeBoard: action,
            setBoardList: flow,
            setBoardProps: action,
            resetBoard: action,
            selectedBoard: action,
            setOneBoard: action,
        });
    }

    get board(){
        return this._board;
    }

    get boards() {
        return toJS(this._boards);
    }

    addBoard(newBoard: BoardType){
        BoardService.createBoard(newBoard);
    }

    modifyBoard = (id: string, board: BoardType) => {
        BoardService.updateBoard(id, this.asNameValues(board));
    }

    removeBoard(id: string){
        BoardService.deleteBoard(id);
    }

    *setBoardList(){
        this._boards = [];
        try{
            yield BoardService.getBoards()
            .then(response => {this._boards = response.data;})
            .catch(error => console.log(error));
        }catch(error){
            console.log(error);
        }
    }

    setBoardProps(name: string, value: string){
        this._board = {
            ...this._board!,
            [name] : value
        }
    }

    resetBoard(){
        // this._board.id = "";
        this._board.title = "";
        this._board.content = "";
        // this._board.writer = "";
        // this._board.hits = 0;
        // this._board.createDate = "";
    }

    selectedBoard(board: BoardType){
        this._board = board;
    }

    setOneBoard(id: string){
        BoardService.getBoardById(id).then(response => {
            runInAction(() => {
                this._board.title = response.data.title;
                this._board.content = response.data.content;
            })
        }).catch(error => console.log(error));
    }

    asNameValues(board: BoardType): NameValueList{
        this.nameValues.push(this.addNameValue('title', board.title));
        this.nameValues.push(this.addNameValue('content', board.content));
        return new NameValueList(...this.nameValues);
    }

    addNameValue(name: string, value: string): NameValue{
        return new NameValue(name, value);
    }
}

export default BoardStore;