import { action, makeObservable, observable } from 'mobx';
import LoginService from '../service/LoginService';
import { RootStore } from './index';
import { toJS } from 'mobx';
import axios from "axios";
import jwt_decode from "jwt-decode";

export type UserInfo = {
    email: string;
    name: string;
}

class LoginStore{
    readonly rootStore: RootStore;
    loggedIn: boolean = false;
    accessToken: string = '';

    constructor(rootStore: RootStore){
        this.rootStore = rootStore;

        makeObservable(this,{
            loggedIn: observable,
            accessToken: observable,
            setLoggedIn: action,
            setAccessToken: action,
        });
    }

    async login(username: string, password: string){
        const result = await LoginService.serverLogin(username, password);
        
        console.log("result: " , result);

        if(result){
            console.log("로그인 성공");
            //axios.defaults.headers.common['Authorization'] = 'Bearer ' + result.data;
            this.setLoggedIn(true);
            return result.data;
            // 로그인 성공
        }
        else{
            console.log("로그인 실패");
            // 로그인 실패
            // 401 비밀번호불일치
            // 500 username 존재X
            throw new Error('Login failed');
        }
    }

    // async logout(){
    //     await serverLogout();
    //     this.setLoggedIn(false);
    // }

    get getLoggedIn(){
        return this.loggedIn;
    }

    setLoggedIn(loggedIn: boolean){
        this.loggedIn = loggedIn;
        if(loggedIn){
            //crmStore.initFromServer();
        }
    }

    setAccessToken(accessToken: string){
        this.accessToken = accessToken;
    }

    decodedToken(token: string): UserInfo{
        return jwt_decode(token);
    }

    getUser(): UserInfo{
        return JSON.parse(localStorage.getItem('userInfo')!);
    }
}

export default LoginStore;