import axios, { AxiosRequestConfig } from "axios";
import { BoardType } from "../stores/BoardStore";
import NameValueList from "../type/NameValueList";

const BOARD_API_BASE_URL = "http://localhost:8090/board";
//const BOARD_API_BASE_URL = "/";

const axiosConfig: AxiosRequestConfig = {
    baseURL: BOARD_API_BASE_URL
}
const client = axios.create(axiosConfig);

class BoardService{

    getBoards(){
        return client.get(BOARD_API_BASE_URL + "/" + "all");
    }

    getBoardById(id: string){
        return client.get(BOARD_API_BASE_URL + "/" + id);
    }

    createBoard(board: BoardType){
        return client.post(BOARD_API_BASE_URL, board);
    }

    updateBoard(id: string, nameValueList: NameValueList){
        //return client.put(BOARD_API_BASE_URL + "/" + id, board);
        return client.put(BOARD_API_BASE_URL + "/" + id, nameValueList);
    }

    deleteBoard(id: string){
        return client.delete(BOARD_API_BASE_URL + "/" + id);
    }
}

export default new BoardService();