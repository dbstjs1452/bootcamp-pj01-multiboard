import axios, { AxiosRequestConfig } from "axios";
import { SignUpForm, UserInfo } from "../stores/UserStore";

const USER_API_BASE_URL = "http://localhost:8090/api/auth";
//const BOARD_API_BASE_URL = "/";

const axiosConfig: AxiosRequestConfig = {
    baseURL: USER_API_BASE_URL
}
const client = axios.create(axiosConfig);

class UserService{

    createUser(user: UserInfo){
        
        return client.post(USER_API_BASE_URL + "/join", user);
    }
}

export default new UserService();