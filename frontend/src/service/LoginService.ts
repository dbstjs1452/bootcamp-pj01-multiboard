import axios, { AxiosRequestConfig } from "axios";


const API_BASE_URL = "http://localhost:8090";

const axiosConfig: AxiosRequestConfig = {
    baseURL: API_BASE_URL
}
const client = axios.create(axiosConfig);

class LoginService{
    
    serverLogin(email: string, password: string){   
        let data = {email: email, password: password}
        return client.post(API_BASE_URL + "/login", data)
        //.then(res => axios.defaults.headers.common['Authorization'] = 'Bearer ' + res.data)        
        .catch(err => console.log("err: ", err));
    }

    serverLogout(){
        return null;
    }

}

export default new LoginService();