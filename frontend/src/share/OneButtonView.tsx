import * as React from 'react';
import {Button, Stack} from '@mui/material';

const OneButtonView = (props: any) => {
  return (
    <>
      <Stack direction='row' justifyContent="center" alignItems="center" spacing={2}>
        <Button
          variant='outlined'
          color='primary'
          fullWidth
          onClick={props.onClickButton}>
          {props.buttonLabel}
        </Button>
      </Stack>
    </>
  );
};

export default OneButtonView;