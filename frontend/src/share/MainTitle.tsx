import * as React from 'react';
import {Typography} from '@mui/material';

const MainTilte = (props: any) => {
    return (
      <>
        <Typography
          sx={{ flex: '1 1 100%', margin:'2rem', textDecoration:'underline wavy aquamarine', fontWeight:'bold'}}
          variant="h5"
          id={props.id}
          component="div"
        >
          {props.titleName}
        </Typography>
      </>
    );
  };
  
  export default MainTilte;