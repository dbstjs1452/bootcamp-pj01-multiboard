import * as React from 'react';
import {Button, Stack} from '@mui/material';

const TwoButtonView = (props: any) => {
  return (
    <>
      <Stack direction='row' justifyContent="center" alignItems="center" spacing={2}>
        <Button
          variant='outlined'
          color='primary'
          fullWidth
          onClick={props.onClickLeftButton}>
          {props.leftLabel}
        </Button>
        <Button
          variant='contained'
          color='primary'
          fullWidth
          onClick={props.onClickRightButton}>
          {props.rightLabel}
        </Button>
      </Stack>
    </>
  );
};

export default TwoButtonView;
