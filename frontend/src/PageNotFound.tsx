import React from 'react';

const PageNotFound = () => {
    return (
        <div>
            <h2>요청하신 페이지를 찾을 수 없습니다!</h2>
            <span>문의사항은 <a href='/qna'>Q&A 게시판</a>을 이용해주세요.</span>
        </div>
    );
};
  
export default PageNotFound;